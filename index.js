

console.log("Hello, World")


/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

	"name" is called a parameter
	A "parameter" acts as a named variable/container that exists only inside of a function
	It is used to store information that is provided to a function when it is called/invoked.
"Juana", the information/data provided directly into the function is called an argument.
	Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.
*/

function printName(name) {
    console.log("My name is "+name);
}

printName("Juana");
printName("John");
printName("Jane"); // observe function reusability

// variables can also be passed as an argument

let sampleVariable = "Yui";
printName(sampleVariable);

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log(`The remainder of ${num} divided by 8 is: ${remainder}`);

    let isDivisibleBy8 = remainder === 0;
    console.log(`Is ${num} divisible by 8?`);
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

//You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

console.log("---------------------------");

function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunctionInParamter){
    argumentFunction();
}

invokeFunction(argumentFunction);

console.log(argumentFunction);

// A function used without a parenthesis is normally associated with using the function as an argument to another function.

// Using multiple arguments

// Multiple arguments will correspond to the number of "parameters" declared in succeeding

function createFullName (firstName, middleName, lastName){
    console.log(`${firstName} ${middleName} ${lastName}`)
}

createFullName("Juan", "Dela", "Cruz");

/*
		"Juan" will be stored in the parameter "firstName"
		"Dela" will be stored in the parameter "middleName"
		"Cruz" will be stored in the parameter "lastName"
		
		----------------------

		In JavaScript, providing more/less arguments than the expected parameters will not return an error.

		Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.

		In other programming languages, this will return an error stating that "the expected number of arguments do not match the number of parameters".
	*/

    createFullName("Juan", "Smith");
    createFullName("Juan", "Smith", "Dimagiba", "Dalisay");

    // variables as arguments
    let firstName = "Cardo";
    let middleName = "Dimagiba";
    let lastName = "Dalisay";

    createFullName(firstName,middleName,lastName);

    function printFullName ( middleName,firstName, lastName){
        console.log(`${firstName} ${middleName} ${lastName}`)
    }
    
    printFullName("Juan", "Dela", "Cruz");

    // results to "Katigbak Christopher Malinao" because "Christopher" was received as middleName, "Katigbak" was received as firstName.
/*
		Parameter names are just names to refer to the argument. Even if we change the name of the parameters, the arguments will be received in the same order it was passed.

		The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.
	*/

    // return statement

    // The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

    
    function returnFullName (firstName, middleName, lastName){
        return `${firstName} ${middleName} ${lastName}`;
    }

    let fullName = returnFullName("Harry", "Beau", "DuBois");
    console.log(fullName);
    
    // function checkIfLegalAge (age) {
    //     return age >= 18;
    // }

    // console.log(checkIfLegalAge(22));

    // create var inside function
    function returnAddress (city, country) {
        let fullAddress = `${city}, ${country}`;
        return fullAddress;
    }

    console.log(returnAddress("Cebu City", "PH"));

    function checkGradeIfWithPrize(score, total){
        let withPrize = score/total;

        return withPrize >= 0.9;
    }

    console.log(checkGradeIfWithPrize(48,50))

    // without return statement

    // On the other hand, when a function the only has console.log() to display its result it will return undefined instead.

    function printPlayerInfo (username, level, job){
        console.log(`Username: ${username}`);
        console.log(`Level: ${level}`);
        console.log(`Job: ${job}`);
        
        // return `${username} | ${level} | ${job}`
    }

    let user1 = printPlayerInfo('ss', 22, 'baker');
    console.log(user1);

    //returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

	//You cannot save any value from printPlayerInfo() because it does not return anything.